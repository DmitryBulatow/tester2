package Test;


import java.io.*;
import java.util.*;

public class Tester {
	private File testFile;
	private Scanner sc;
	private  int questionCountInTest;
	private ArrayList<String> testText;
	private Calendar calendar;
	private PrintWriter printWriter;
	private double points;
	private ArrayList<Question> questionsOrder;
	private ArrayList<String> printAsFalse;


	public Tester(File testFile) {
		this.testFile = testFile;
		try {
			printWriter = new PrintWriter(testFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		printAsFalse = new ArrayList<>();

	}

	public int getQuestionCountInTest() {
		return questionCountInTest;
	}

	public boolean ready() {
		try {
			sc = new Scanner(testFile);
		} catch (FileNotFoundException e) {
			return false;
		}


		if (sc.hasNextInt()){
			questionCountInTest = sc.nextInt();
			if (sc.hasNext()){
				String s = sc.next();
				if (s.trim().equals("GO") || s.trim().equals("go") || s.trim().equals("Go") || s.trim().equals("gO")){
					return  true;
				}
			}
		}
		return false;
	}

	public void doTest(QuestionsBaze questionsBaze) {
		createTest(questionsBaze);
		writeTest();
		timeOn();
		while (notYet()){
			//nothing to do
		}
		checkTest();
		writeTestingQuality();
	}

	private boolean notYet() {
		try {
			sc = new Scanner(testFile);
		} catch (FileNotFoundException e) {
			return true;
		}
		if (sc.hasNext()){
			return !sc.next().trim().equals("All");
		}
		return true;
	}
	private void writeTestingQuality() {
		printWriter.println("\n\n==============TEST IS OVER==================");
		printWriter.println("Your points = " + points);
		int maxPoints = 0;
		for (int i = 0; i < questionsOrder.size(); i++) {
			maxPoints += questionsOrder.get(i).getCost();
		}
		printWriter.println("Max points = " + maxPoints);
		Calendar calendar1 = new GregorianCalendar();
		printWriter.print("Testing time: ");
		if (calendar.get(Calendar.MINUTE) - calendar1.get(Calendar.MINUTE) != 0){
			printWriter.print(Math.abs(calendar.get(Calendar.MINUTE) - calendar1.get(Calendar.MINUTE)) + " min, ");
		}

		printWriter.println(Math.abs(calendar.get(Calendar.SECOND)-calendar1.get(Calendar.SECOND)) + " sec\n");


		if (Objects.nonNull(printAsFalse)){
			for (int i = 0; i < printAsFalse.size(); i++) {
				printWriter.println(printAsFalse.get(i));
			}
		}
		printWriter.flush();
	}

	private void timeOn() {

		calendar = new GregorianCalendar();
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MINUTE,0);
	}

	private void writeTest() {
		for (int i = 0; i < testText.size(); i++) {
			printWriter.println(testText.get(i));
		}
		printWriter.flush();
	}

	private void checkTest() {
		checkPoints(readAnswers());
	}

	private int[][] readAnswers() {
		try {
			sc = new Scanner(testFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int[][] peopleAnswers = new int[getQuestionCountInTest()][];
		int number = 0;
		for (int i = 0; i < testText.size() && sc.hasNext(); i++) {
			String stringRead = sc.nextLine();
			if (stringRead.contains("Your answer(s):")){
				String rtp = stringRead.replace("Your answer(s):","");
				peopleAnswers[number] = new int[rtp.split(",").length];
				for (int j = 0; j < rtp.split(",").length; j++) {
					peopleAnswers[number][j] = Integer.parseInt(rtp.split(",")[j].trim());


				}
				number++;
			}
		}
		return peopleAnswers;
	}

	private void checkPoints(int[][] peopleAnswers) {//[номер задания][i-й ответ]
		points=0.0;
		for (int i = 0; i < peopleAnswers.length; i++) {
			double tempPoints = 0.0;
			if (Objects.nonNull(peopleAnswers[i])){
				for (int j = 0; j < peopleAnswers[i].length; j++) {
					if (peopleAnswers[i][j]-1>0 && peopleAnswers[i][j]-1<= questionsOrder.get(i).getAnswersText().length){
						tempPoints += questionsOrder.get(i).getAnswers()[peopleAnswers[i][j]-1].getAnswerCost();
					}
				}
				if (tempPoints!=questionsOrder.get(i).getCost()){
					writeTrueAnswers(questionsOrder.get(i), peopleAnswers[i]);
				}
				if (tempPoints < 0){
					tempPoints=0;
				}
				if (tempPoints > questionsOrder.get(i).getCost()){
					tempPoints = questionsOrder.get(i).getCost();
				}
			}
			points+=tempPoints;
		}
	}

	private void writeTrueAnswers(Question question, int[] peopleAnswers) {
		printAsFalse.add(question.getQuestion());
		for (int i = 0; i <question.getAnswers().length; i++) {
			printAsFalse.add(question.getAnswers()[i].getAnswerText());
		}
		StringBuilder temp = new StringBuilder("Your answers:");
		for (int i = 0; i < peopleAnswers.length; i++) {
			temp.append((peopleAnswers[i])+ ",");
		}
		printAsFalse.add("\n" + (temp));
		temp = new StringBuilder("Write answers:");
		for (int i = 0; i < question.getAnswers().length; i++) {
			if (question.getAnswers()[i].isAnswer()){
				temp.append((question.getAnswers()[i].getNumber() + ","));
			}
		}
		printAsFalse.add((temp)+"\n");
	}


	private void createTest(QuestionsBaze questionsBaze) {
		questionsOrder = new ArrayList<>();
		testText = new ArrayList<>();
		testText.add(" ");
		testText.add("Write \"All\" on the first line to finish. \":\" - multi question.");
		testText.add("       Questions cost it's number in square brackets [].          ");
		testText.add("          Write your answers separated by commas                  ");
		testText.add("                       Good luck!                               \n");
		boolean[] thisQuestionWas = new boolean[questionsBaze.getQuestionsCount()];
		int i = 0;
		while (i< questionCountInTest){
			int temp = (int) Math.round(Math.random()*(questionsBaze.getQuestionsCount()-1));
			if (!thisQuestionWas[temp]){
				questionsOrder.add(questionsBaze.getQuestion(temp));
				testText.add("[" + questionsBaze.getQuestion(temp).getQuestion());
				for (int j = 0; j < questionsBaze.getQuestion(temp).getAnswersText().length; j++) {
					testText.add((questionsBaze.getQuestion(temp).getAnswers()[j].getNumber()) + ")" + questionsBaze.getQuestion(temp).getAnswers()[j].getAnswerText());
				}
				i++;
				thisQuestionWas[temp] = true;
				testText.add("Your answer(s):");
				testText.add("");
			}
		}
	}

	public void countQuestionsError(int questionsCount) {

		if (Objects.nonNull(printWriter)){
			if (questionsCount <= 0){
				printWriter.println("LOL=)");
			}else {
				printWriter.println("In our database only " + questionsCount + " questions");
			}
			printWriter.println("clean up this text and write true. Thanks");
			printWriter.flush();
		}
	}

}
