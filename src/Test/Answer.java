package Test;

public class Answer {
	private String answerText;
	private boolean isAnswer;
	private double answerCost;
	private int number;

	public Answer(String answerText, boolean isAnswer, double answerCost, int number) {
		this.answerText = answerText;
		this.isAnswer = isAnswer;
		this.answerCost = isAnswer? answerCost: -answerCost;
		cleanIsAnswer();
		this.number = number;
	}

	private void cleanIsAnswer() {
		StringBuilder sb = new StringBuilder(answerText);
		if(sb.charAt(0)=='F' || sb.charAt(0)=='T' || sb.charAt(0)=='f' || sb.charAt(0)=='t'){
			sb.delete(0, 2);
		}
		answerText = sb.toString();
	}

	public boolean isAnswer() {
		return isAnswer;
	}

	public double getAnswerCost() {
		return answerCost;
	}

	public String getAnswerText() {
		return answerText;
	}

	public int getNumber() {
		return number;
	}
}
