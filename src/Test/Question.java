package Test;


public class Question {

	private int cost;
	private String question;
	private Answer[] answers;
	private String[] fullText;
	private String[] answersText;
	private double answerCost;
	private int answersCount;

	public String[] getAnswersText() {
		return answersText;
	}

	public int getAnswersCount() {
		return answersText.length;
	}

	public Question(String[] text) {
		this.answers = new Answer[text.length-1];
		this.question = text[0];

		this.fullText = new String[text.length];
		fullText[0] = question;

		answersText = new String[text.length-1];
		for (int i = 0; i < text.length-1; i++) {
			answersText[i] = text[i+1];
		}

		searchCost(text);

		createAnswers();

	}

	private void createAnswers() {
		for (int i = 0; i < answers.length; i++) {
			answers[i] = new Answer(answersText[i],answersText[i].trim().toCharArray()[0]=='T'
					|| answersText[i].trim().toCharArray()[0]=='t', answerCost
					,i+1);
		}


		for (int i = 1; i < answers.length+1; i++) {
			fullText[i] = answers[i-1].getAnswerText();
		}
	}

	private void searchCost(String[] text) {
		int tempCost = 0;
		for (int i = 0; i < text[0].length() && text[0].charAt(i)!=']' && text[0].charAt(i)!=' '; i++) {
			tempCost = tempCost*10 + (text[0].charAt(i) - '0');
		}

		cost = tempCost;
		this.answerCost = cost;

		if (question.trim().charAt(question.length()-1)==':'){
			double countAnswers = 0;
			for (int i = 1; i < text.length; i++) {
				countAnswers += text[i].trim().charAt(0)=='t'||text[i].trim().charAt(0)=='T'? 1:0;
			}
			answerCost = cost/countAnswers;
		}
	}


	public String[] getFullText() {
		return fullText;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Answer[] getAnswers() {
		return answers;
	}

	public void setAnswers(Answer[] answers) {
		this.answers = answers;
	}
}
