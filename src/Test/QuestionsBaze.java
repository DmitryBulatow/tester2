package Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class QuestionsBaze {


	private final File questionsFile;
	private ArrayList<Question> question;
	private Scanner sc;
	private int questionsCount;

	public Question getQuestion(int i) {
		return question.get(i);
	}

	public QuestionsBaze(File questionsFile) {
		question = new ArrayList<>();
		this.questionsFile = questionsFile;
		try {
			sc = new Scanner(questionsFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}




	public boolean haveQuestions() {

		return sc.hasNextLine();

	}

	public void checkNewQuestions(){
		while (sc.hasNextLine()) {

			ArrayList<String> temp = new ArrayList<>();
			while (sc.hasNextLine()) {
				String t = sc.nextLine();
				if (!t.trim().equals("")){
					temp.add(t);
				}else{
					break;
				}
			}
			String[] tt = new String[temp.size()];
			for (int i = 0; i < tt.length; i++) {
				tt[i] = temp.get(i);
			}
			question.add(new Question(tt));

			questionsCount++;

		}

	}

	public int getQuestionsCount() {

		return questionsCount;
	}

	public void closeScanner(){
		sc.close();
	}


}
