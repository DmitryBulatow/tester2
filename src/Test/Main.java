package Test;
import java.io.File;

public class Main {


	public static void main(String[] args) {
		String separator = File.separator;
		String testerFileName = "D:"+ separator+ "Users"+separator+"JAVA"+separator+"TEST.txt";
		Tester tester = new Tester(new File("TEST.txt"));

		String questionsFileName = "D:"+ separator+ "ITIS"+separator+"JAVA"+separator+"questions.txt";
		QuestionsBaze questionsBaze = new QuestionsBaze(new File("questions.txt"));

		boolean was = true;
		while (true){

			questionsBaze.checkNewQuestions();

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (questionsBaze.getQuestionsCount()>0 && tester.ready()){


				if (questionsBaze.getQuestionsCount() >= tester.getQuestionCountInTest() && questionsBaze.getQuestionsCount()!=0){
					tester.doTest(questionsBaze);
					was = true;
				}else if (was){
					tester.countQuestionsError(questionsBaze.getQuestionsCount());
					was = false;
				}
			}
		}
	}
}
